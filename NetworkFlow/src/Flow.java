import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

class FileReader {
    public Node[] read(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        Node[] nodes = new Node[Integer.parseInt(scanner.nextLine())];
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = new Node(i, scanner.nextLine());
        }
        scanner.nextLine();
        // Read until capacity
        while (scanner.hasNextLine()) {
            String[] splits = scanner.nextLine().split(" ");
            if (splits.length == 3) {
                nodes[Integer.parseInt(splits[0])].addConnection(Integer.parseInt(splits[1]), Integer.parseInt(splits[2]), Type.FORWARD);
                if (Integer.parseInt(splits[0]) != 0) {
                    nodes[Integer.parseInt(splits[1])].addConnection(Integer.parseInt(splits[0]), Integer.parseInt(splits[2]), Type.FORWARD);
                }
            } else throw new IllegalStateException("Incorrect edge format");
        }
        return nodes;
    }

}

class Node {
    Integer id;
    String name;
    ArrayList<ResidualEdge> connections = new ArrayList<>();

    public Node(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Node(Integer id, Integer foreignId, Integer capacity, Type type) {
        this.id = id;
        connections.add(new ResidualEdge(foreignId, capacity, type));
    }

    public void addConnection(Integer foreignId, Integer capacity, Type type) {
        connections.add(new ResidualEdge(foreignId, capacity, type));
    }

    @Override
    public String toString() {
        return id + " " + name + " ";
    }
}

class ResidualEdge {
    Integer destinationId;
    Integer capacity;
    Integer flow;
    Type type;


    public ResidualEdge(Integer destinationId, Integer capacity, Type type) {
        this.destinationId = destinationId;
        this.capacity = capacity < 0 ? Integer.MAX_VALUE : capacity;
        this.flow = 0;
        this.type = type;
    }

    public void incFlow(Integer flow) {
        this.flow += flow;
        this.capacity -= flow;
    }

    public void incCapacity(Integer capacity) {
        this.capacity += capacity;
    }

    @Override
    public String toString() {
        return destinationId + " " + capacity + " " + flow;
    }

}

enum Type {
    BACKWARD, FORWARD;
}

class Algorithm {
    public Node[] nodes;
    public static final Integer DESTINATION_NODE = 54;

    public Algorithm(Node[] nodes) {
        this.nodes = nodes;
        solve();
    }

    public void solve() {
        Integer bottleNeck = 0;
        Integer sum = 0;
        ArrayList<LinkedList<Integer>> paths = new ArrayList<>();
        List<Integer> cut = new LinkedList<>();
        do {
            LinkedList<Integer> path = new LinkedList<>();
            bottleNeck = findPath(nodes[0], null, Integer.MAX_VALUE, path);
            sum += bottleNeck;
            if (bottleNeck > 0)
                paths.add(path);
        } while (bottleNeck > 0);
        findCut(nodes[0], cut);
        getCutValue(cut);
        System.out.println(sum);
    }

    private void getCutValue(List<Integer> cut) {
        Integer sum = 0;
        for (Integer nodeId : cut) {
            Node node = nodes[nodeId];
            for (ResidualEdge residualEdge : node.connections) {
                if(cut.contains(residualEdge.destinationId))
                    continue;
                if ((residualEdge.capacity == 0 && residualEdge.flow > 0 && residualEdge.type != Type.BACKWARD)) {
                    sum += residualEdge.flow;
                    System.out.println(nodeId + " " + residualEdge.destinationId + " " + residualEdge.flow);
                }
            }
        }
        System.out.println(sum);
    }

    public void findCut(Node node, List<Integer> path) {
        path.add(node.id);
        for (ResidualEdge residualEdge : node.connections) {
            if (path.contains(residualEdge.destinationId))
                continue;
            if (residualEdge.capacity > 0) {
                findCut(nodes[residualEdge.destinationId], path);
            }
        }
    }

    public Integer findPath(Node source, Node previous, Integer flow, LinkedList<Integer> path) {
        if (DESTINATION_NODE == source.id) {
            return flow;
        }
        if (path.contains(source.id)) {
            return 0;
        }
        for (ResidualEdge residualEdge : source.connections) {
            if (residualEdge.capacity < 1)
                continue;
            Node nextNode = nodes[residualEdge.destinationId];
            flow = residualEdge.capacity < flow ? residualEdge.capacity : flow;
            path.add(source.id);
            Integer bottleNeck = findPath(nextNode, source, flow, path);
            if (bottleNeck > 0) {
                residualEdge.incFlow(bottleNeck);
                if (previous != null) {
                    Optional<ResidualEdge> undoFlow = nextNode.connections.stream().filter(a -> a.type == Type.BACKWARD && a.destinationId == source.id).findFirst();
                    if (!undoFlow.isPresent()) {
                        nextNode.connections.add(new ResidualEdge(source.id, bottleNeck, Type.BACKWARD));
                    } else undoFlow.get().incCapacity(bottleNeck);
                }
                return bottleNeck;
            }
        }
        path.remove(source.id);
        return 0;
    }
}

public class Flow {
    public static void main(String[] args) throws FileNotFoundException {
        FileReader fileReader = new FileReader();
        File file = new File(args[0]);
        Node[] nodes = fileReader.read(file);
        Algorithm algorithm = new Algorithm(nodes);
    }
}
