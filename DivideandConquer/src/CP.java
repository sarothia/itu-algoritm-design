import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

class FileReader {
    public ArrayList<Point> read(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        ArrayList<Point> points = new ArrayList<>();

        while (scanner.hasNext()) {
            String[] splits = scanner.nextLine().trim().split("\\s+(?=[-]?\\d+)");

            if (splits.length == 3)
                points.add(new Point(splits[0], Double.parseDouble(splits[1]), Double.parseDouble(splits[2])));
        }

        return points;
    }
}

class Point {
    String name;
    Double x;
    Double y;

    public Point(String name, Double x, Double y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }
}

class Algorithm {
    List<Point> byX;
    Pair<Point, Point> pair;
    Double minDist = Double.MAX_VALUE;

    public Algorithm(ArrayList<Point> points) {
        byX = points.stream().sorted((o1, o2) -> o1.x.compareTo(o2.x)).collect(Collectors.toList());
        getClosestPair(0, byX.size());
    }

    private static List<Point> merge(List<Point> a, List<Point> b) {
        List<Point> answer = new ArrayList<>();
        int i = 0, j = 0;

        while (i < a.size() && j < b.size()) {
            if (a.get(i).y < b.get(j).y)
                answer.add(a.get(i++));
            else
                answer.add(b.get(j++));
        }

        while (i < a.size())
            answer.add(a.get(i++));

        while (j < b.size())
            answer.add(b.get(j++));

        return answer;
    }

    private List<Point> getClosestPair(Integer from, Integer to) {
        if (3 > Math.abs(from - to)) {
            List<Point> list = new ArrayList<>();
            for (int i = from; i < to; i++) {
                for (int j = i + 1; j < to; j++) {
                    double distance = calculateDistance(byX.get(i), byX.get(j));
                    if (distance < minDist) {
                        minDist = distance;
                        pair = new Pair<>(byX.get(i), byX.get(j));
                    }
                }
                list.add(byX.get(i));
            }
            list.sort((o1, o2) -> o1.y.compareTo(o2.y));
            return list;
        }

        Integer line = (from + to) / 2;

        List<Point> leftPair = getClosestPair(from, line),
                rightPair = getClosestPair(line, to),
                merged = merge(leftPair, rightPair),
                withinDelta = merged.stream().filter(point -> Math.abs(point.x - byX.get(line).x) < minDist).collect(Collectors.toList());

        for (int i = 0; i < withinDelta.size(); i++) {
            for (int j = i + 1; j < withinDelta.size() && Math.abs(i - j) < 12; j++) {
                double distance = calculateDistance(withinDelta.get(i), withinDelta.get(j));
                if (distance < minDist) {
                    minDist = distance;
                    pair = new Pair<>(withinDelta.get(i), withinDelta.get(j));
                }
            }
        }

        return merged;
    }

    private Double calculateDistance(Point i, Point j) {
        return Math.sqrt(Math.pow((i.x - j.x), 2) + Math.pow((i.y - j.y), 2));
    }
}

public class CP {

    private static void run(File file) throws IOException {
        FileReader fileReader = new FileReader();

        ArrayList<Point> points = fileReader.read(file);
        Algorithm algorithm = new Algorithm(points);

        String distance = algorithm.minDist.equals((double) algorithm.minDist.intValue()) ? ((Integer) algorithm.minDist.intValue()).toString() : algorithm.minDist.toString();
        System.out.println("..\\" + file.getParentFile().getName() + "\\" + file.getName() + ": " + points.size() + " " + distance);
    }

    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        if (file.isDirectory()) {
            Files.walk(Paths.get(args[0])).forEach(filePath -> {
                if (filePath.getFileName().toString().contains("tsp")) {
                    try {
                        run(filePath.toFile());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            run(file);
        }
    }
}
