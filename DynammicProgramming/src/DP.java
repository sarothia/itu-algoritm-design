import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class BLOSUM62 {

    public static final int[][] matrix = new int[][]{
        /*A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  */
        /*A*/{4, -2, 0, -2, -1, -2, 0, -2, -1, -4, -1, -1, -1, -2, -4, -1, -1, -1, 1, 0, 0, 0, -3, 0, -2, -1, -4},
        /*B*/ {-2, 4, -3, 4, 1, -3, -1, 0, -3, -4, 0, -4, -3, 3, -4, -2, 0, -1, 0, -1, -3, -3, -4, -1, -3, 1, -4},
        /*C*/ {0, -3, 9, -3, -4, -2, -3, -3, -1, -4, -3, -1, -1, -3, -4, -3, -3, -3, -1, -1, 9, -1, -2, -2, -2, -3, -4},
        /*D*/ {-2, 4, -3, 6, 2, -3, -1, -1, -3, -4, -1, -4, -3, 1, -4, -1, 0, -2, 0, -1, -3, -3, -4, -1, -3, 1, -4},
        /*E*/ {-1, 1, -4, 2, 5, -3, -2, 0, -3, -4, 1, -3, -2, 0, -4, -1, 2, 0, 0, -1, -4, -2, -3, -1, -2, 4, -4},
        /*F*/ {-2, -3, -2, -3, -3, 6, -3, -1, 0, -4, -3, 0, 0, -3, -4, -4, -3, -3, -2, -2, -2, -1, 1, -1, 3, -3, -4},
        /*G*/ {0, -1, -3, -1, -2, -3, 6, -2, -4, -4, -2, -4, -3, 0, -4, -2, -2, -2, 0, -2, -3, -3, -2, -1, -3, -2, -4},
        /*H*/ {-2, 0, -3, -1, 0, -1, -2, 8, -3, -4, -1, -3, -2, 1, -4, -2, 0, 0, -1, -2, -3, -3, -2, -1, 2, 0, -4},
        /*I*/ {-1, -3, -1, -3, -3, 0, -4, -3, 4, -4, -3, 2, 1, -3, -4, -3, -3, -3, -2, -1, -1, 3, -3, -1, -1, -3, -4},
        /*J*/ {-4, -4, -4, -4, -4, -4, -4, -4, -4, 1, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4},
        /*K*/ {-1, 0, -3, -1, 1, -3, -2, -1, -3, -4, 5, -2, -1, 0, -4, -1, 1, 2, 0, -1, -3, -2, -3, -1, -2, 1, -4},
        /*L*/ {-1, -4, -1, -4, -3, 0, -4, -3, 2, -4, -2, 4, 2, -3, -4, -3, -2, -2, -2, -1, -1, 1, -2, -1, -1, -3, -4},
        /*M*/ {-1, -3, -1, -3, -2, 0, -3, -2, 1, -4, -1, 2, 5, -2, -4, -2, 0, -1, -1, -1, -1, 1, -1, -1, -1, -1, -4},
        /*N*/ {-2, 3, -3, 1, 0, -3, 0, 1, -3, -4, 0, -3, -2, 6, -4, -2, 0, 0, 1, 0, -3, -3, -4, -1, -2, 0, -4},
        /*O*/ {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, 1, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4},
        /*P*/ {-1, -2, -3, -1, -1, -4, -2, -2, -3, -4, -1, -3, -2, -2, -4, 7, -1, -2, -1, -1, -3, -2, -4, -2, -3, -1, -4},
        /*Q*/ {-1, 0, -3, 0, 2, -3, -2, 0, -3, -4, 1, -2, 0, 0, -4, -1, 5, 1, 0, -1, -3, -2, -2, -1, -1, 3, -4},
        /*R*/ {-1, -1, -3, -2, 0, -3, -2, 0, -3, -4, 2, -2, -1, 0, -4, -2, 1, 5, -1, -1, -3, -3, -3, -1, -2, 0, -4},
        /*S*/ {1, 0, -1, 0, 0, -2, 0, -1, -2, -4, 0, -2, -1, 1, -4, -1, 0, -1, 4, 1, -1, -2, -3, 0, -2, 0, -4},
        /*T*/ {0, -1, -1, -1, -1, -2, -2, -2, -1, -4, -1, -1, -1, 0, -4, -1, -1, -1, 1, 5, -1, 0, -2, 0, -2, -1, -4},
        /*U*/ {0, -3, 9, -3, -4, -2, -3, -3, -1, -4, -3, -1, -1, -3, -4, -3, -3, -3, -1, -1, 9, -1, -2, -2, -2, -3, -4},
        /*V*/ {0, -3, -1, -3, -2, -1, -3, -3, 3, -4, -2, 1, 1, -3, -4, -2, -2, -3, -2, 0, -1, 4, -3, -1, -1, -2, -4},
        /*W*/ {-3, -4, -2, -4, -3, 1, -2, -2, -3, -4, -3, -2, -1, -4, -4, -4, -2, -3, -3, -2, -2, -3, 11, -2, 2, -3, -4},
        /*X*/ {0, -1, -2, -1, -1, -1, -1, -1, -1, -4, -1, -1, -1, -1, -4, -2, -1, -1, 0, 0, -2, -1, -2, -1, -1, -1, -4},
        /*Y*/ {-2, -3, -2, -3, -2, 3, -3, 2, -1, -4, -2, -1, -1, -2, -4, -3, -1, -2, -2, -2, -2, -1, 2, -1, 7, -2, -4},
        /*Z*/ {-1, 1, -3, 1, 4, -3, -2, 0, -3, -4, 1, -3, -1, 0, -4, -1, 3, 0, 0, -1, -3, -2, -3, -1, -2, 4, -4},
            /**
             *
             */
            {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, -4, 1}
    };

    private static int getDistance(int i, int j) {
        if (i < 0 || i > matrix[0].length) {
            throw new RuntimeException();
        }
        if (j < 0 || j > matrix[0].length) {
            throw new RuntimeException();
        }
        return matrix[i][j];
    }

    public static int calcAlignment(char i, char j) {
        return getDistance(i - 65, j - 65); // ASCII based index to letter
    }
}

class FileReader {
    public ArrayList<DNA> read(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);
        ArrayList<DNA> dnas = new ArrayList<>();
        String name = "";
        String dna = "";

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.startsWith(">")) {
                if (name.length() > 0) {
                    dnas.add(new DNA(name, dna));
                    dna = "";
                }
                name = line.split(" ")[0].substring(1);
            } else {
                dna += line;
            }
        }
        dnas.add(new DNA(name, dna));

        return dnas;
    }
}

class DNA {

    String name;
    String dna;

    public DNA(String name, String dna) {
        this.name = name;
        this.dna = dna;
    }
}

class DNAPair {
    public Node endNode;
    public DNA OrgDNA;
    public DNA ComDNA;

    public DNAPair(Node endNode, DNA orgDNA, DNA comDNA) {
        this.endNode = endNode;
        OrgDNA = orgDNA;
        ComDNA = comDNA;
    }

    @Override
    public String toString() {
        return OrgDNA.name + "--" + ComDNA.name + ": " + endNode.value + "\n";
    }
}

class Node {
    int value;
    Node previous;
    char comparedChar;
    char originalChar;

    public Node(int value, Node previous, char comparedChar, char originalChar) {
        this.value = value;
        this.previous = previous;
        this.comparedChar = comparedChar;
        this.originalChar = originalChar;
    }
}

class Algorithm {
    public ArrayList<DNAPair> comparedDNAs = new ArrayList<>();
    int spacePenalty = -4;

    public Algorithm(ArrayList<DNA> dnas) {
        for (int i = 0; i < dnas.size() - 1; i++) {
            DNA original = dnas.get(i);

            for (int j = i + 1; j < dnas.size(); j++) {
                DNA compared = dnas.get(j);

                Node end = compareDNA(original.dna, compared.dna);
                comparedDNAs.add(new DNAPair(end, original, compared));
            }
        }
    }

    private Node compareDNA(String original, String compared) {
        //init nodes
        Node[][] nodeMatrix = new Node[compared.length() + 1][original.length() + 1];
        nodeMatrix[0][0] = new Node(0, null, '-', '-');

        //fill edges
        fillVerticalEdge(original, nodeMatrix);
        fillHorizontalEdge(compared, nodeMatrix);

        // Calculate matrix values
        for (int i = 1; i < nodeMatrix.length; i++) {
            for (int j = 1; j < nodeMatrix[i].length; j++) {
                nodeMatrix[i][j] = max(nodeMatrix[i - 1][j - 1], nodeMatrix[i - 1][j], nodeMatrix[i][j - 1], compared.charAt(i - 1), original.charAt(j - 1));
            }
        }

        return nodeMatrix[nodeMatrix.length - 1][nodeMatrix[0].length - 1];
    }

    private void fillHorizontalEdge(String compared, Node[][] m) {
        for (int i = 1; i <= compared.length(); i++) {
            m[i][0] = new Node(m[i - 1][0].value + spacePenalty, m[i - 1][0], '-', compared.charAt(i - 1));
        }
    }

    private void fillVerticalEdge(String original, Node[][] m) {
        for (int i = 1; i <= original.length(); i++) {
            m[0][i] = new Node(m[0][i - 1].value + spacePenalty, m[0][i - 1], original.charAt(i - 1), '-');
        }
    }

    private Node max(Node dia, Node hor, Node ver, char i, char j) {
        Node max = null;
        int diacost = dia.value + BLOSUM62.calcAlignment(i, j);
        int horcost = hor.value + spacePenalty;
        int vercost = ver.value + spacePenalty;

        if (diacost > horcost) {
            max = new Node(diacost, dia, i, j);
        } else {
            max = new Node(horcost, hor, i, '-');
        }

        if (max.value > vercost) {
            return max;
        } else {
            return new Node(vercost, ver, '-', j);
        }
    }
}

public class DP {
    private static void printResult(Node acc, boolean isOrg) {
        if (acc.previous == null) {
            return;
        } else {
            printResult(acc.previous, isOrg);
            if (isOrg) {
                System.out.print(acc.comparedChar);
            } else {
                System.out.print(acc.originalChar);
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        FileReader fileReader = new FileReader();
        File file = new File(args[0]);
        ArrayList<DNA> dnas = fileReader.read(file);
        Algorithm algorithm = new Algorithm(dnas);

        algorithm.comparedDNAs.stream().forEach(dnaPair -> {
            System.out.print(dnaPair);
            printResult(dnaPair.endNode, true);
            System.out.println("");
            printResult(dnaPair.endNode, false);
            System.out.println("");
        });
    }
}
