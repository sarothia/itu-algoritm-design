import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

class FileReader {
    public void read(File file, List<Person> persons){
        try {
            Scanner scanner = new Scanner(file);
            int numberOfPairs = 0;
            String line = "";
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                if (line.startsWith("#") || isBlank(line)) continue;
                else break;
            }

            if (line.startsWith("n"))
                numberOfPairs = Integer.parseInt(line.substring(2));

            readPersons(scanner, numberOfPairs, persons);
            scanner.nextLine();
            readPreferences(scanner, numberOfPairs, persons);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void readPersons(Scanner scanner, int numberOfPairs, List<Person> persons){
        for (int t = 0; t < numberOfPairs * 2; t++) {
            String line = scanner.nextLine();
            if(isBlank(line) || line.length() < line.indexOf("\\s"))
                throw new IllegalStateException("Incorrect number of people");

            Person person = new Person(line.substring(line.indexOf(" ")+1));
            persons.add(person);
        }
    }

    private void readPreferences(Scanner scanner, int numberOfPairs, List<Person> persons){
        for (int t = 0; t < numberOfPairs * 2; t++) {
            String line = scanner.nextLine();
            if (isBlank(line) || line.length() < line.indexOf(":") + 1)
                throw new IllegalStateException("Incorrect number of people");

            int index = (Integer.parseInt(line.substring(0, line.indexOf(':'))) - 1);
            Person person = persons.get(index);
            if (person == null)
                throw new IllegalStateException("Illegal index");

            Integer[] preferences = new Integer[numberOfPairs];
            person.invPreferences = new Integer[numberOfPairs * 2];
            line = line.substring(line.indexOf(":") + 2);

            List<String> values = Arrays.asList(line.split("\\s"));
            for (int i = 0; i < numberOfPairs; i++) {
                if (isBlank(values.get(i)))
                    throw new IllegalStateException("Incorrect number of preferences");
                person.preferences.addLast(Integer.parseInt(values.get(i)) - 1);
                person.invPreferences[person.preferences.getLast()] = i;
            }
        }
    }
    public static boolean isBlank(CharSequence cs) {
        int strLen;
        if (cs != null && (strLen = cs.length()) != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(cs.charAt(i))) {
                    return false;
                }
            }
            return true;
        } else {
            return true;
        }
    }
}

class Person {
    public LinkedList<Integer> preferences = new LinkedList<Integer>();
    public String name;
    public Integer[] invPreferences;
    public Integer partnerIndex = null;

    Person(String name){
        this.name = name;
    }
}

class Algo {
    public void matchPairs(List<Person> persons) {
        for (int i = 0; i < persons.size(); i+=2) {
            Integer separatedMale = null;
            do {
                separatedMale = findPartner(separatedMale != null ? separatedMale:i, persons);
            } while(separatedMale != null);
        }
    }

    public Integer findPartner(Integer personIndex, List<Person> persons){
        Person person = persons.get(personIndex);
        Person partner;
        Integer partnerIndex;

        do {
            partnerIndex = person.preferences.pop();
            partner = persons.get(partnerIndex);
        } while (partner.partnerIndex != null && (partner.invPreferences[partner.partnerIndex] < partner.invPreferences[personIndex]));

        Integer oldPartnerIndex = partner.partnerIndex;
        person.partnerIndex = partnerIndex;
        partner.partnerIndex = personIndex;

        return oldPartnerIndex;
    }
}

public class GS {
    public static void main(String [] args)
    {
        FileReader fileReader = new FileReader();
        List<Person> persons = new ArrayList<Person>();
        Algo algo = new Algo();
        File file = new File(args[0]);

        fileReader.read(file, persons);

        algo.matchPairs(persons);

        for (Integer i = 0; persons.size() > i; i+=2) {
            Person male = persons.get(i);
            Person female = persons.get(male.partnerIndex);
            System.out.println(male.name + " -- " + female.name);
        }
    }
}
