import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

class FileReader {

    public void read(File file, List<Interval> intervals) {
        try {
            Scanner scanner = new Scanner(file);
            int numberOfIntervals;
            String line;

            line = scanner.nextLine();
            numberOfIntervals = Integer.parseInt(line);
            scanner.nextLine();

            readIntervals(scanner, numberOfIntervals, intervals);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void readIntervals(Scanner scanner, int numberOfIntervals, List<Interval> intervals) {
        for (int i = 0; i < numberOfIntervals; i++) {
            Interval interval = new Interval(scanner.nextInt(), scanner.nextInt());
            intervals.add(interval);
        }
    }
}

class Interval implements Comparable<Interval> {

    public final Integer start;
    public final Integer end;
    private Integer room;

    Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public void setRoom(Integer room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return start + " " + end + " " + room;
    }

    @Override
    public int compareTo(Interval o) {
        return start.compareTo(o.start);
    }
}

class Room implements Comparable<Room>{
    public Integer number;
    public Integer availableTime;

    public Room(Integer availableTime, Integer number) {
        this.number = number;
        this.availableTime = availableTime;
    }

    @Override
    public int compareTo(Room o) {
        return availableTime.compareTo(o.availableTime);
    }
}

class Algorithm {
    public Integer roomCount = 0;

    public Algorithm(ArrayList<Interval> intervals) {
        PriorityQueue<Room> pq = new PriorityQueue<>();

        intervals.stream().sorted().collect(Collectors.toList()).stream().forEach((currentInterval) -> {
            if (pq.peek() == null) {
                int size = pq.size();
                currentInterval.setRoom(size);
                pq.add(new Room(currentInterval.end, size));
            } else {
                if (pq.peek().availableTime <= currentInterval.start) {
                    Room room = pq.poll();
                    currentInterval.setRoom(room.number);
                    room.availableTime = currentInterval.end;
                    pq.add(room);
                } else {
                    int size = pq.size();
                    Room room = new Room(currentInterval.end, size);
                    currentInterval.setRoom(size);
                    pq.add(room);
                }
            }
        });
        roomCount = pq.size();
    }
}

public class IP {

    private static void printResults(int roomCount, ArrayList<Interval> intervals) {
        System.out.println(roomCount);
        System.out.println("");
        intervals.stream().forEach(System.out::println);
    }

    public static void main(String[] args) {
        FileReader fileReader = new FileReader();
        ArrayList<Interval> intervals = new ArrayList<>();
        File file = new File(args[0]);

        fileReader.read(file, intervals);
        Algorithm algorithm = new Algorithm(intervals);

        printResults(algorithm.roomCount, intervals);
    }
}
